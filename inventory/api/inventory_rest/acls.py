from .keys import PEXELS_API_KEY
import json
import requests


def get_car_pic(make, model):
    headers = {"Authorization": PEXELS_API_KEY}

    params = {"query": make + " " + model}

    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    parsed_json = json.loads(response.content)
    picture = parsed_json["photos"][0]["src"]["original"]
    return {"picture_url": picture}
