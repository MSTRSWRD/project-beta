from django.urls import path
from .views import api_list_technicians, api_technician, api_list_appointments, api_appointment

urlpatterns= [
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path("technicians/<int:pk>/", api_technician, name="api_technician"),
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path("appointments/<int:pk>/", api_appointment, name="api_appointment"),
    path("appointments/<int:pk>/cancel", api_appointment, name="api_appointment" ),
    path("appointments/<int:pk>/finish", api_appointment, name="api_appointment" ),

]
