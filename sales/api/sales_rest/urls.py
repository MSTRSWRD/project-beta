from django.urls import path

from .views import (
    list_salespeople_api,
    list_customers_api,
    list_sales_api,
    delete_salesperson_api,
    delete_customer_api,
    delete_sales_api
)


urlpatterns = [
    path("salespeople/", list_salespeople_api, name="list_salespeople"),
    path("salespeople/<int:pk>", delete_salesperson_api, name="delete_salespeople"),
    path("customers/", list_customers_api, name="list_customers"),
    path("customers/<int:pk>", delete_customer_api, name="delete_customer"),
    path("sales/", list_sales_api, name="list_sales"),
    path("sales/<int:pk>", delete_sales_api, name="delete_sales"),
]
