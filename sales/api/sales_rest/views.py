from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json 
from .models import AutomobileVO, Customer, Sale, Salesperson
from .encoders import CustomerEncoder, SaleEncoder, SalespersonEncoder

# Create your views here.

@require_http_methods(["GET", "POST"])
def list_salespeople_api(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create sales person"}
            )
            response.status_code = 400
            return response
        

@require_http_methods(["DELETE", "GET"])
def delete_salesperson_api(request, pk):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(id=pk)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist!"})
            response.status_code = 404
            return response
    else:
        try:
            salesperson = Salesperson.objects.get(id=pk)
            salesperson.delete()
            return JsonResponse(
                {"message": "Delete was successful!"}
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Does not exist!"})


@require_http_methods(["GET", "POST"])
def list_customers_api(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create customer"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET"])
def delete_customer_api(request, pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist!"})
            response.status_code = 404
            return response
    else:
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                {"message": "Delete was successful!"}
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist!"})


@require_http_methods(["GET", "POST"])
def list_sales_api(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            automobile_id = content["automobile_id"]
            salesperson_id = content["salesperson_id"]
            customer_id = content["customer_id"]

            automobile = AutomobileVO.objects.get(pk=automobile_id)
            salesperson = Salesperson.objects.get(pk=salesperson_id)
            customer = Customer.objects.get(pk=customer_id)
            
            content["automobile"] = automobile
            content["salesperson"] = salesperson
            content["customer"] = customer
            sale = Sale.objects.create(**content)
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except Exception as e:
            print(e)
            response = JsonResponse(
                {"message": "Could not create sale", "error": e}
            )
            response.status_code = 400
            return response  
        
@require_http_methods(["DELETE", "GET"])
def delete_sales_api(request, pk):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=pk)
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False
            )
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Does not exist!"})
            response.status_code = 404
            return response
    else:
        try:
            sale = Sale.objects.get(id=pk)
            sale.delete()
            return JsonResponse(
                {"message": "Delete was successful!"}
            )
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Does not exist!"})
        