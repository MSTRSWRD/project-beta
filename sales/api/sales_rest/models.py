from django.db import models
from django.urls import reverse
# Create your models here.
'''
sales needs to keep track of automobile sales that come from the invetory
    - a person can not sell a car that i not listed in the invetory not repeat a sale on same car
'''


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Salesperson(models.Model):
    first_name = models.CharField(max_length=100) 
    last_name = models.CharField(max_length=100) 
    employee_id = models.CharField(max_length=55, unique=True)

    def __str__(self):
        return self.first_name + " " + self.last_name


class Customer(models.Model):
    first_name = models.CharField(max_length=100) 
    last_name = models.CharField(max_length=100) 
    address = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=20)

    def __str__(self):
        return self.first_name + " " + self.last_name


class Sale(models.Model):
    price = models.DecimalField(max_digits=10, decimal_places=2)
    
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales",
        on_delete=models.CASCADE,

    )

    salesperson = models.ForeignKey(
        Salesperson,
        related_name="sales",
        on_delete=models.CASCADE,
    )

    customer = models.ForeignKey(
        Customer,
        related_name="sales",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("list_sales", kwargs={"pk": self.pk})
    