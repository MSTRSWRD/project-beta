import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useState, useEffect } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import ListManufacturers from './ListManufacturers';
import AddManufacturerForm from './AddManufacturerForm';
import ListModels from './ListModels';
import AddVehicleModel from './AddVehicleModel';
import ListAutomobiles from './ListAutomobiles';
import AddAutomobileForm from './AddAutomobileForm';
import ListTechnicians from './ListTechnicians';
import AddTechnicianForm from './AddTechnicianForm';
import AddSalespersonForm from './AddSalespersonForm';
import ListSalespeople from './ListSalespeople';
import AddCustomerForm from './AddCustomerForm';
import ListCustomers from './ListCustomers';
import AddRecordSaleForm from './AddRecordSaleForm';
import ListSales from './ListSales';
import SalesHistory from './ShowSalesHistory';
import AddAppointmentForm from './AddAppointmentForm';
import ListAppointments from './ListAppointments';
import ServiceHistoryList from './ServiceHistory'


function App() {
  const [color, setColor] = useState(false);
  
  const click = () => {
    setColor(!color)
  }
  useEffect(() => {
    document.body.style.backgroundColor = color
  }, [color])

  return (
    <>
    <BrowserRouter>
      <Nav />
       {/* <div >
          <button onClick={click} className={`btn-sm ${(color === true)? 'btn-primary' : 'btn-light'}`}
          >Light/Dark Mode</button>
        </div> */}
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="manufacturers">
            <Route index element={<ListManufacturers />} />
            <Route path="new" element={<AddManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route index element={<ListModels />} />
            <Route path="new" element={<AddVehicleModel />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<ListAutomobiles />} />
            <Route path="new" element={<AddAutomobileForm />} />
          </Route>
          <Route path="salespeople">
            <Route index element={<ListSalespeople />} />
            <Route path="new" element={<AddSalespersonForm />} />
          </Route>
          <Route path="customers">
            <Route index element={<ListCustomers />} />
            <Route path="new" element={<AddCustomerForm />} />
          </Route>
          <Route path="sales">
            <Route index element={<ListSales />} />
            <Route path="new" element={<AddRecordSaleForm />} />
          </Route>
          <Route path="sales/history" element={<SalesHistory />}/>
          <Route path="technicians">
            <Route index element={<ListTechnicians />} />
            <Route path="new" element={<AddTechnicianForm />} />
          </Route>
          <Route path="appointments">
            <Route index element={<ListAppointments />} />
            <Route path="new" element={<AddAppointmentForm />} />
          </Route>
          <Route path="servicehistory" element={<ServiceHistoryList />}/>
        </Routes>
      </div>
    </BrowserRouter>
    </ >
  );
}

export default App;
