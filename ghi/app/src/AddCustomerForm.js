import React, { useState } from 'react';


function AddCustomerForm() {
    const [first_name, setFirst] = useState('');
    const [last_name, setLast] = useState('');
    const [address, setAddress] = useState('');
    const [phone_number, setPhoneNumber] = useState('');

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.first_name = first_name;
        data.last_name = last_name;
        data.address = address;
        data.phone_number = phone_number;

        const url = 'http://localhost:8090/api/customers/';
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch (url, fetchOptions);
        if (response.ok) {
            setFirst('');
            setLast('');
            setAddress('');
            setPhoneNumber('');
        }
    }

    const handleFirstNameChange = (e) => {
        const value = e.target.value;
        setFirst(value);
    }
    const handleLastNameChange = (e) => {
        const value = e.target.value;
        setLast(value);
    }
    const handleAddressChange = (e) => {
        const value = e.target.value;
        setAddress(value);
    }
    const handlePhoneNumberChange = (e) => {
        const value = e.target.value;
        setPhoneNumber(value);
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Customer</h1>
            <form onSubmit={handleSubmit} id="add-customer-form">
              <div className="form-floating mb-3">
                <input value={first_name} onChange={handleFirstNameChange} placeholder="First name..." required type="text" name="first_name" id="first_name" className="form-control" />
                <label htmlFor="first_name">First name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={last_name} onChange={handleLastNameChange} placeholder="Last name..." required type="text" name="last_name" id="last_name" className="form-control" />
                <label htmlFor="last_name">Last name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={address} onChange={handleAddressChange} placeholder="Address..." required type="text" name="address" id="address" className="form-control" />
                <label htmlFor="address">Address</label>
              </div>
              <div className="form-floating mb-3">
                <input value={phone_number} onChange={handlePhoneNumberChange} placeholder="Phone number..." required type="number" name="phone_number" id="phone_number" className="form-control" />
                <label htmlFor="phone_number">Phone number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}
export default AddCustomerForm;