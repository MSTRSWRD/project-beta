import React, {useState, useEffect} from 'react';

function AddRecordSaleForm() {

    const [autos, setAutos] = useState([]);
    const [vin, setVin] = useState('');
    const [automobile_id, setAuto] = useState('');
    const [salespeople, setSalespeople] = useState([]);
    const [salesperson_id, setSalesperson] = useState('');
    const [customers, setCustomers] = useState([]);
    const [customer_id, setCustomer] = useState('');
    const [price, setPrice] = useState('');

    const fetchAutos = async () => {
        const autosUrl = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(autosUrl);

        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos);
        }
    }
    const fetchSalespeople = async () => {
        const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(salespeopleUrl);

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        }
    }
    const fetchCustomers = async () => {
        const customersUrl = 'http://localhost:8090/api/customers/';
        const response = await fetch(customersUrl);

        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        }
    }

    useEffect(()=> {
        fetchAutos();
        fetchSalespeople();
        fetchCustomers();
    }, []);

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.price = price;
        data.automobile_id = automobile_id;
        data.salesperson_id = salesperson_id;
        data.customer_id = customer_id;

        const salesUrl = 'http://localhost:8090/api/sales/';
        const salesFetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const salesResponse = await fetch(salesUrl, salesFetchOptions);
        if (salesResponse.ok) {
            const autosUrl = `http://localhost:8100/api/automobiles/${vin}/`;
            const autosFetchOptions = {
                method: 'put',
                body: JSON.stringify({"sold": true}),
                headers: {
                    'Content-Type': 'application/json'
                },
            };
            const autosResponse = await fetch(autosUrl, autosFetchOptions);
            if (autosResponse.ok) {
                setPrice('');
                setAuto('');
                setSalesperson('');
                setCustomer('');
            }
        }

    }
   

    const handleAutoChange = (e) => {
        const value = e.target.value;
        setAuto(value);
        autos.filter(car => car.id == value).map(car => {
            setVin(car.vin)
        })

    }
    const handleSalespersonChange = (e) => {
        const value = e.target.value;
        setSalesperson(value);
    }
    const handleCustomerChange = (e) => {
        const value = e.target.value;
        setCustomer(value);
    }
    const handlePriceChange = (e) => {
        const value = e.target.value;
        setPrice(value);
    }


    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Record a new sale</h1>
                <form onSubmit={handleSubmit} id="add-recorded-sale-form">
                    <div className="mb-3">
                        Automobile VIN
                        <select onChange={handleAutoChange} value={automobile_id}  required name="automobile_id" id="automobile_id" className="form-select">
                            <option value="">Choose a automobile VIN...</option>
                            {autos.filter(car => car.sold === false).map(car => {
                                return(
                                    <option key={car.id} value={car.id}>
                                        {car.vin}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    <div className="mb-3">
                        Salesperson
                        <select onChange={handleSalespersonChange} value={salesperson_id} required name="salesperson_id" id="salesperson_id" className="form-select">
                            <option value="">Choose a salesperson...</option>
                            {salespeople.map(employee => {
                                return(
                                    <option key={employee.id} value={employee.id}>
                                        {employee.first_name }, {employee.last_name}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    <div className="mb-3">
                        Customer
                        <select onChange={handleCustomerChange} value={customer_id} required name="customer_id" id="customer_id" className="form-select">
                            <option value="">Choose a customer...</option>
                            {customers.map(client => {
                                return(
                                    <option key={client.id} value={client.id}>
                                        {client.first_name}, {client.last_name}
                                    </option>
                                    );
                                })}
                            </select>
                        </div>
                    <div className="mb-3">
                        Price
                        <input onChange={handlePriceChange} value={price} placeholder="0" required type="number" name="price"
                            id="price" className="form-control" />
                        <label htmlFor="price"></label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
);
}

export default AddRecordSaleForm;
