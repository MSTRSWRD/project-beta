import { useEffect, useState } from 'react';
import dayjs from "dayjs";

function AppointmentList() {
    const [appointments, setAppointments] = useState([]);
    const [autos, setAutos] = useState([]);

    const fetchData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
            }
        };

    const autoData = async () => {
        const autoresponse = await fetch('http://localhost:8100/api/automobiles/');
        if (autoresponse.ok) {
            const autodata = await autoresponse.json();
            setAutos(autodata.autos);
            }
        };

    useEffect(() => {
        fetchData();
        autoData();
    }, []);

    const isVIP = (vin) => {
        const vipLookup = autos.find((auto) => auto.vin === vin);
        return vipLookup ? true : false;
    };

    const finishAppt = async (apptID) => {
        const response = await fetch(`http://localhost:8080/api/appointments/${apptID}/finish`, {
            method: 'PUT',
            body: JSON.stringify({"status": "finished"}),
            headers: {
                'Content-Type': "application/json"
            }
        });

        if (response.ok) {
            setAppointments((prevAppointments) => {
                return prevAppointments.map((appointment) => {
                    if (appointment.id === apptID) {
                        return {
                            appointment,
                            status: "finished"
                    };
                }
                return appointment;
            });
        });
        } else {
            console.error('Failed to complete appt');
        }
    };

    const cancelAppt = async (apptID) => {
        const response = await fetch(`http://localhost:8080/api/appointments/${apptID}/cancel`, {
            method: 'PUT',
            body: JSON.stringify({"status": "cancelled"}),
            headers: {
                'Content-Type': "application/json"
            }
        });

        if (response.ok) {
            setAppointments((prevAppointments) => {
                return prevAppointments.map((appointment) => {
                    if (appointment.id === apptID) {
                        return {
                            appointment,
                            status: "cancelled"
                    };
                }
                return appointment;
            });
        });
        } else {
            console.error('Failed to cancel appt');
        }
    };

    return (
        <div>
            <h1>Service Appointments</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {appointments
                    .filter((appointment) => appointment.status === "created")
                    .sort((a, b) => dayjs(a.date_time).valueOf() - dayjs(b.date_time).valueOf())
                    .map(appointment => {
                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{isVIP(appointment.vin) ? 'VIP' : 'no'}</td>
                                <td>{appointment.customer}</td>
                                <td>{dayjs(appointment.date_time).format("MM/DD/YYYY")}</td>
                                <td>{dayjs(appointment.date_time).format("h:mm:ss A")}</td>
                                <td>{appointment.technician.first_name + " " + appointment.technician.last_name}</td>
                                <td>{appointment.reason}</td>
                                <td>
                                    <button onClick={() => finishAppt(appointment.id)} type="button" className="btn btn-success">Complete</button>
                                    <button onClick={() => cancelAppt(appointment.id)} type="button" className="btn btn-danger">Cancel</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default AppointmentList;
