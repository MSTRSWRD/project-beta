import { useEffect, useState } from 'react';

function ListSalespeople() {

    const [first_name, setFirst] = useState('');
    const [last_name, setLast] = useState('');
    const [employee_id, setEmployeeID] = useState('');

    const [salespeople, setSalespeople] = useState([]);
    const fetchData = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        }
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.first_name = first_name;
        data.last_name = last_name;
        data.employee_id = employee_id;

        const url = 'http://localhost:8090/api/salespeople/';
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch (url, fetchOptions);
        if (response.ok) {
            setFirst('');
            setLast('');
            setEmployeeID('');
        }
    }

    const handleFirstNameChange = (e) => {
        const value = e.target.value;
        setFirst(value);
    }
    const handleLastNameChange = (e) => {
        const value = e.target.value;
        setLast(value);
    }
    const handleEmployeeIDChange = (e) => {
        const value = e.target.value;
        setEmployeeID(value);
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-md-2">
                <div className="list-group shadow-sm small mb-3">
                  <div className="list-group-item active mt-3">Salespeople Data</div>
                  <a 
                    href="#" 
                    className="list-group-item list-group-item-action"
                    data-bs-toggle="modal"
                    data-bs-target="#add-salesperson-form">
                    Add Salesperson
                  </a>
                </div>
              </div>
              <div className="col-md-10">
                <div className="card shadow-sm border-0 mb-3">
                  <div className="card-header text-white text-center mt-3 bg-dark">Salespeople List</div>
                  <table className="table table-sm small table-bordered align-middle mb-0">
                    <thead className='table-secondary'>
                      <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                    <tbody>
                      {salespeople.map(employee => {
                        return (
                          <tr key={employee.id}>
                            <td>{employee.employee_id}</td>
                            <td>{employee.first_name}</td>
                            <td>{employee.last_name}</td>
                            <td className='d-grid'>
                              <a
                                href="#"
                                className='btn btn-sm btn-danger'
                                data-bs-toggle='modal'
                                data-bs-target='#salesperson_delete'>
                                Delete
                              </a>
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          {/* Add Salesperson Modal */}
          <div 
            className="modal fade"
            id='add-salesperson-form'
            data-bs-backdrop='static'
            tabindex='-1'
            aria-hidden='true'
            style={{ backdropFilter: 'blur(5px);' }} >
            <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header bg-black">
                        <h5 className="modal-title text-white ">Add Salesperson Details</h5>
                        <button
                            type='button'
                            className='btn-close btn-close-white'
                            data-bs-dismiss='modal'
                            aria-label='Close'>
                        </button>
                    </div>
                <div className="modal-body">
                <form onSubmit={handleSubmit} id="add-salesperson-form">
                  <div className="container">
                    <div className="row gx-3">
                      <div className="col-md-12">
                        <div className="mb-3">
                          <input value={first_name} onChange={handleFirstNameChange} placeholder="First name..." required type="text" name="first_name" id="first_name" className="form-control" />
                          <label htmlFor="first_name">First name</label>
                        </div>
                      </div>
                      <div className="col-md-12">
                        <div className="mb-3">
                          <input value={last_name} onChange={handleLastNameChange} placeholder="Last name..." required type="text" name="last_name" id="last_name" className="form-control" />
                          <label htmlFor="last_name">Last name</label>
                        </div>
                      </div>
                      <div className="col-md-12">
                        <div className="mb-3">
                          <input value={employee_id} onChange={handleEmployeeIDChange} placeholder="Employee ID..." required type="text" name="employee_id" id="employee_id" className="form-control" />
                          <label htmlFor="employee_id">Employee ID</label>
                        </div>
                        <div className="col-md-10">
                            <div className="d-grid">
                                <button 
                                    type='submit'
                                    className='btn btn-sm btn-success'>
                                    Add Salesperson
                                </button>
                            </div>
                        </div>
                        <div className="col-md-2 mt-3">
                            <div className="d-grid">
                                <button 
                                    type='button'
                                    className='btn btn-sm btn-warning'
                                    data-bs-dismiss='modal'
                                    aria-label='Close'>
                                        Close
                                </button>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              </div>
            </div>
          </div>
        </>
      );      
 }
export default ListSalespeople;

