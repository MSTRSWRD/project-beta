import pexels_video from './assets/pexles-car-video.mp4'

function MainPage() {
  return (
      <div className='main' id='video'> 
        <div className='overlay'>
          <video src={pexels_video} type='video/mp4' autoPlay loop muted />
          <div className='content'>
            <h1 className="display-5 fw-bold">CarCar</h1>
            <p className="lead mb-4">
            The premiere solution for automobile dealership
            management!
            </p>
            </div>
          </div>
      </div>
  );
}

export default MainPage;