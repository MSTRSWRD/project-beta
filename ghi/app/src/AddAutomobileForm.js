import React, {useState, useEffect} from 'react';

function NewAutomobileForm() {
    const [color, setColor] = useState('');
    const [year, setYear] = useState('');
    const [vin, setVin] = useState('');
    const [model_id, setModel_id] = useState('');
    const [models, setModels] = useState([]);


    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    }

    useEffect(()=> {
        fetchData();
    }, []);

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = model_id;

        const autoUrl = 'http://localhost:8100/api/automobiles/';
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(autoUrl, fetchOptions);
        if (response.ok) {
            setColor('');
            setYear('');
            setVin('');
            setModel_id('');
        }
    }

    const handleColorChange = (e) => {
        const value = e.target.value;
        setColor(value);
    }
    const handleYearChange = (e) => {
        const value = e.target.value;
        setYear(value);
    }
    const handleVinChange = (e) => {
        const value = e.target.value;
        setVin(value);
    }
    const handleModelChange = (e) => {
        const value = e.target.value;
        setModel_id(value);
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add an automobile to inventory</h1>
                <form onSubmit={handleSubmit} id="add-automobile-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="Color"
                            id="Color" className="form-control" />
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleYearChange} value={year} placeholder="Year" required type="text" name="year"
                            id="year" className="form-control" />
                        <label htmlFor="year">Year</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleVinChange} value={vin} placeholder="Vin" required type="text" name="vin"
                            id="vin" className="form-control" />
                        <label htmlFor="vin">Vin</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleModelChange} value={model_id} required name="model_id" id="model_id" className="form-select">
                            <option value="">Choose a model</option>
                            {models.map(model => {
                                return(
                                    <option key={model.id} value={model.id}>
                                        {model.name}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
);
}

export default NewAutomobileForm;
