import React, {useEffect, useState} from 'react';

function NewAppointmentForm() {
    const [date_time, setDateTime] = useState('');
    const [reason, setReason] = useState('');
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [technician_id, setTechnician] = useState('');
    const [technicians, setTechnicians] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    }

    useEffect(()=> {
        fetchData();
    }, []);

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.date_time = date_time;
        data.reason = reason;
        data.vin = vin;
        data.customer = customer;
        data.technician_id = technician_id;

        const apptUrl = 'http://localhost:8080/api/appointments/';
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch (apptUrl, fetchOptions);
        if (response.ok) {
            setDateTime('');
            setReason('');
            setVin('');
            setCustomer('');
            setTechnician('');
        }
    }

    const handleDateTimeChange = (e) => {
        const value = e.target.value;
        setDateTime(value);
    }
    const handleReasonChange = (e) => {
        const value = e.target.value;
        setReason(value);
    }
    const handleVinChange = (e) => {
        const value = e.target.value;
        setVin(value);
    }
    const handleCustomerChange = (e) => {
        const value = e.target.value;
        setCustomer(value);
    }
    const handleTechnicianChange = (e) => {
        const value = e.target.value;
        setTechnician(value);
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Schedule a Service Appointment</h1>
                <form onSubmit={handleSubmit} id="add-appointment-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleVinChange} value={vin} placeholder="vin" required type="text" name="vin"
                            id="vin" className="form-control" />
                        <label htmlFor="vin">Automobile Vin</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleCustomerChange} value={customer} placeholder="customer" required type="text" name="customer"
                            id="customer" className="form-control" />
                        <label htmlFor="customer">Customer</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleDateTimeChange} value={date_time} placeholder="date_time" required type="datetime-local" name="date_time"
                            id="date_time" className="form-control" />
                        <label htmlFor="date_time">Date</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleReasonChange} value={reason} placeholder="reason" required type="text" name="reason"
                            id="reason" className="form-control" />
                        <label htmlFor="reason">Reason</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleTechnicianChange} value={technician_id} required name="technician_id" id="technician_id" className="form-select">
                            <option value="">Choose a technician</option>
                            {technicians.map(technician => {
                                return(
                                    <option key={technician.id} value={technician.id}>
                                        {technician.first_name + " " + technician.last_name}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
    );

}

export default NewAppointmentForm;
