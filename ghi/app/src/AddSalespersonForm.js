import React, { useState } from 'react';


function AddSalespersonForm() {
    const [first_name, setFirst] = useState('');
    const [last_name, setLast] = useState('');
    const [employee_id, setEmployeeID] = useState('');

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.first_name = first_name;
        data.last_name = last_name;
        data.employee_id = employee_id;

        const url = 'http://localhost:8090/api/salespeople/';
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch (url, fetchOptions);
        if (response.ok) {
            setFirst('');
            setLast('');
            setEmployeeID('');
        }
    }

    const handleFirstNameChange = (e) => {
        const value = e.target.value;
        setFirst(value);
    }
    const handleLastNameChange = (e) => {
        const value = e.target.value;
        setLast(value);
    }
    const handleEmployeeIDChange = (e) => {
        const value = e.target.value;
        setEmployeeID(value);
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Salesperson</h1>
            <form onSubmit={handleSubmit} id="add-salesperson-form">
              <div className="form-floating mb-3">
                <input value={first_name} onChange={handleFirstNameChange} placeholder="First name..." required type="text" name="first_name" id="first_name" className="form-control" />
                <label htmlFor="first_name">First name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={last_name} onChange={handleLastNameChange} placeholder="Last name..." required type="text" name="last_name" id="last_name" className="form-control" />
                <label htmlFor="last_name">Last name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={employee_id} onChange={handleEmployeeIDChange} placeholder="Employee ID..." required type="text" name="employee_id" id="employee_id" className="form-control" />
                <label htmlFor="employee_id">Employee ID</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}
export default AddSalespersonForm;