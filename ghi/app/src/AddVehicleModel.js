import React, { useEffect, useState } from 'react';

function AddVehicleModelForm() {

    const [name, setName] = useState('');
    // const [picture_url, setPictureURL] = useState('');
    const [manufacturer_id, setManufacturer] = useState('');
    const [manufacturers, setManufacturers] = useState([]);


    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.manufacturer_id = manufacturer_id;
        data.name = name;
        // data.picture_url = picture_url;

        const url = 'http://localhost:8100/api/models/';

        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };


        const response = await fetch(url, fetchOptions);

        if (response.ok) {
            setManufacturer('');
            setName('');
            // setPictureURL('');
        }
    }

    const handleManufacturerChange = (e) => {
        const value = e.target.value;
        setManufacturer(value);
    }
    const handleNameChange = (e) => {
        const value = e.target.value;
        setName(value);
    }
    // const handlePictureURLChange = (e) => {
    //     const value = e.target.value;
    //     setPictureURL(value);
    // }



    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a vehicle model</h1>
                    <form onSubmit={handleSubmit} id="add-vehicle-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} value={name} placeholder="Model Name" required type="text" name="name"
                                id="name" className="form-control" />
                            <label htmlFor="name">Model Name</label>
                        </div>
                        {/* <div className="form-floating mb-3">
                            <input onChange={handlePictureURLChange} value={picture_url} placeholder="Picture URL" required type="url" name="picture_url"
                                id="picture_url" className="form-control" />
                            <label htmlFor="picture_url">Picture URL</label>
                        </div> */}
                        <div className="mb-3">
                            <select onChange={handleManufacturerChange} value={manufacturer_id} required name="manufacturer_id" id="manufacturer_id" className="form-select">
                                <option value="">Choose a manufacturer...</option>
                                {manufacturers.map(manufacturer => {
                                    return(
                                        <option key={manufacturer.id} value={manufacturer.id}>
                                            {manufacturer.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}


export default AddVehicleModelForm;
