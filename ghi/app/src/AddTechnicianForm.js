import React, {useState} from 'react';


function NewTechnicianForm() {
    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('');
    const [employee_id, setEmployeeID] = useState('');



    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.first_name = first_name;
        data.last_name = last_name;
        data.employee_id = employee_id;

        const techUrl = 'http://localhost:8080/api/technicians/';
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch (techUrl, fetchOptions);
        if (response.ok) {
            setFirstName('');
            setLastName('');
            setEmployeeID('');
        }
    }

    const handleFirstNameChange = (e) => {
        const value = e.target.value;
        setFirstName(value);
    }
    const handleLastNameChange = (e) => {
        const value = e.target.value;
        setLastName(value);
    }
    const handleEmployeeIDChange = (e) => {
        const value = e.target.value;
        setEmployeeID(value);
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add a Technician</h1>
                <form onSubmit={handleSubmit} id="add-technician-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleFirstNameChange} value={first_name} placeholder="First name..." required type="text" name="first_name"
                            id="first_name" className="form-control" />
                        <label htmlFor="first_name">First Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleLastNameChange} value={last_name} placeholder="Last name..." required type="text" name="last_name"
                            id="last_name" className="form-control" />
                        <label htmlFor="last_name">Last Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleEmployeeIDChange} value={employee_id} placeholder="Employee ID..." required type="number" name="employee_id"
                            id="employee_id" className="form-control" />
                        <label htmlFor="employee_id">Employee ID</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
    );

}

export default NewTechnicianForm;
