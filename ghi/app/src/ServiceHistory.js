import { useEffect, useState } from 'react';
import dayjs from "dayjs";

function ServiceHistoryList() {
    const [appointments, setAppointments] = useState([]);
    const [autos, setAutos] = useState([]);
    const [filterValue, setFilterValue] = useState('');


    const fetchData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
            }
        };

    const autoData = async () => {
        const autoresponse = await fetch('http://localhost:8100/api/automobiles/');
        if (autoresponse.ok) {
            const autodata = await autoresponse.json();
            setAutos(autodata.autos);
            }
        };

    useEffect(() => {
        fetchData();
        autoData();
    }, []);

    const isVIP = (vin) => {
        const vipLookup = autos.find((auto) => auto.vin === vin);
        return vipLookup ? true : false;
    };

    const handleFilterChange = (e) => {
        setFilterValue(e.target.value.toUpperCase())
    }



    return (
        <div>
            <h1>Service History</h1>
            <input onChange={handleFilterChange} style={{ width: "250px "}} placeholder='Search by VIN'/>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments
                    .filter(appointment => appointment.vin
                    .includes(filterValue))
                    .map(appointment => {
                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{isVIP(appointment.vin) ? 'VIP' : 'no'}</td>
                                <td>{appointment.customer}</td>
                                <td>{dayjs(appointment.date_time).format("MM/DD/YYYY")}</td>
                                <td>{dayjs(appointment.date_time).format("h:mm:ss A")}</td>
                                <td>{appointment.technician.first_name + " " + appointment.technician.last_name}</td>
                                <td>{appointment.reason}</td>
                                <td>{appointment.status}</td>
                            </tr>
                        )
                    })
                }
                </tbody>
            </table>
        </div>
    )
}

export default ServiceHistoryList;
